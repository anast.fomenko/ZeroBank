## Automation tests for Zero bank website

## How to use?

1. Clone project `git clone https://gitlab.com/anast.fomenko/ZeroBank.git`
2. Install dependencies `npm install`
3. Run tests `npm run test`
