import {LoginPage} from "../pageObjects/loginPage";
import {MyMoneyMapPage} from "../pageObjects/myMoneyMapPage";

describe('Work with diagram', () => {
  it('My money map', async () => {
    jest.setTimeout(50000);
    let loginPage = new LoginPage();
    let myMoneyPage = new MyMoneyMapPage();
    await loginPage.visit();
    await loginPage.loginToZeroBanck('username', 'password');
    await myMoneyPage.openMyMoneyMapPage();
    await myMoneyPage.workWithTableAndOutFlowChart();
  })
})
