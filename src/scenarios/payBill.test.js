import { LoginPage } from '../pageObjects/loginPage';
import {PayBillsPage} from "../pageObjects/payBillsPage";


describe('ZeroBank', () => {
    it('Pay bills', async () => {
        jest.setTimeout(20000);
        let loginPage = new LoginPage();
        let payBillsPage = new PayBillsPage();
        await loginPage.visit();
        await loginPage.loginToZeroBanck('username', 'password');
        await payBillsPage.openPayBillForm();
        await payBillsPage.fillAndSaveBillsForm();
    })
})
