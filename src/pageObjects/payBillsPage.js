export class PayBillsPage {
  async openPayBillForm() {
    await page.click('#pay_bills_tab');
    await page.waitForSelector('.board');
  }

  async fillAndSaveBillsForm() {
    await page.select('#sp_payee', 'Apple');
    await page.select('#sp_account', 'Loan');
    await page.type('#sp_amount', '320');
    await page.type('#sp_date', '2020-03-20');
    await page.keyboard.press('Enter');
    await page.type('#sp_description', 'Test');
    await page.click('#pay_saved_payees');
    await page.waitForSelector('#alert_content');
  }
}

