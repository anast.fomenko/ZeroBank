export class LoginPage {
  async visit() {
    await page.screenshot({path: "screenshot/page.png", fullPage: true});
    await page.goto("http://zero.webappsecurity.com/login.html");
  }

  async loginToZeroBanck(username, password) {
    const userLogin = "#user_login";
    const userPassword = "#user_password";
    const submitButton = "input[type='submit']";

    await page.type(userLogin, username);
    await page.type(userPassword, password);
    await page.click(submitButton);
    await page.waitForSelector('.nav-tabs');
  }
}

