export class MyMoneyMapPage {
  async openMyMoneyMapPage() {
    await page.click('#money_map_tab');
    await page.waitForSelector('#zeroviewport-1009-body');
  }

  async workWithTableAndOutFlowChart() {
    await page.click('#gridcolumn-1019-titleEl');
    await page.waitForSelector('text[text="OutFlow Chart"]');
    await page.click('text:nth-of-type(12)');
    await page.click('text:nth-of-type(13)');
    await page.click('text:nth-of-type(14)');
    await page.click('text:nth-of-type(15)');
  }
}
